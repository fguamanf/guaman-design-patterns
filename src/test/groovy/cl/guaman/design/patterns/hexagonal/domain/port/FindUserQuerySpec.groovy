package cl.guaman.design.patterns.hexagonal.domain.port

import cl.guaman.design.patterns.hexagonal.domain.User
import cl.guaman.design.patterns.hexagonal.infraestructure.adapter.FindUserAdapter
import cl.guaman.design.patterns.hexagonal.infraestructure.mock.UserMock
import spock.lang.Specification

/**
 * @author Fabián Guamán
 */
class FindUserQuerySpec extends Specification {

    UserMock userMock
    FindUserQuery findUserQuery

    def setup() {
        userMock = new UserMock()
        findUserQuery = new FindUserAdapter(userMock)
    }

    def "find all users"() {
        when:
        List<User> users = findUserQuery.findAll()
        then:
        users.size() == 3
    }
}
