package cl.guaman.design.patterns.hexagonal.domain.usecase

import cl.guaman.design.patterns.hexagonal.common.enumeration.Channel
import cl.guaman.design.patterns.hexagonal.common.enumeration.TypeMessage
import cl.guaman.design.patterns.hexagonal.common.exception.LogicErrorException
import cl.guaman.design.patterns.hexagonal.domain.command.MessageSendCommand
import cl.guaman.design.patterns.hexagonal.domain.port.FindUserQuery
import cl.guaman.design.patterns.hexagonal.domain.port.SendMessagePort
import cl.guaman.design.patterns.hexagonal.domain.validation.MessageSendCommandValidation
import cl.guaman.design.patterns.hexagonal.domain.validation.ObjectValidation
import cl.guaman.design.patterns.hexagonal.infraestructure.adapter.FindUserAdapter
import cl.guaman.design.patterns.hexagonal.infraestructure.adapter.SendMessageAdapter
import cl.guaman.design.patterns.hexagonal.infraestructure.mock.EmailMock
import cl.guaman.design.patterns.hexagonal.infraestructure.mock.PhoneMock
import cl.guaman.design.patterns.hexagonal.infraestructure.mock.UserMock
import spock.lang.Specification

/**
 * @author Fabián Guamán
 */
class NotifyUserUseCaseSpec extends Specification {

    UserMock userMock
    FindUserQuery findUserQuery
    EmailMock emailMock
    PhoneMock phoneMock
    SendMessagePort sendMessagePort
    NotifyUserUseCase notifyUserUseCase
    ObjectValidation<MessageSendCommand> messageSendCommandObjectValidation

    def setup() {
        userMock = new UserMock()
        findUserQuery = new FindUserAdapter(userMock)
        emailMock = new EmailMock()
        phoneMock = new PhoneMock()
        sendMessagePort = new SendMessageAdapter(emailMock, phoneMock)
        messageSendCommandObjectValidation = new MessageSendCommandValidation()
        notifyUserUseCase = new NotifyUserUseCase(
                findUserQuery,
                sendMessagePort,
                messageSendCommandObjectValidation
        )
    }

    def "send messages success"(MessageSendCommand messageSendCommand, Object result) {
        expect:
        notifyUserUseCase.notify(messageSendCommand) == result
        where:
        messageSendCommand                        | result
        ["1", Channel.ALL, TypeMessage.SUCCESS]   | null
        ["1", Channel.ALL, TypeMessage.WARNING]   | null
        ["2", Channel.EMAIL, TypeMessage.SUCCESS] | null
        ["3", Channel.PHONE, TypeMessage.SUCCESS] | null
    }

    def "send messages error"(MessageSendCommand messageSendCommand) {
        when:
        notifyUserUseCase.notify(messageSendCommand)
        then:
        def error = thrown(exception)
        error.message == message
        where:
        messageSendCommand                      | exception           | message
        ["4", Channel.ALL, TypeMessage.SUCCESS] | LogicErrorException | "user not found"
        null                                    | LogicErrorException | "ERROR_FORM"
        []                                      | LogicErrorException | "ERROR_FORM"
    }
}
