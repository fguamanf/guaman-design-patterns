package cl.guaman.design.patterns.hexagonal.domain.command;

import cl.guaman.design.patterns.hexagonal.common.enumeration.Channel;
import cl.guaman.design.patterns.hexagonal.common.enumeration.TypeMessage;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Fabián Guamán
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MessageSendCommand {

    private String userId;
    private Channel channel;
    private TypeMessage typeMessage;
}
