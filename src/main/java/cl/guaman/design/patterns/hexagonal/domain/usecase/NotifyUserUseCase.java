package cl.guaman.design.patterns.hexagonal.domain.usecase;

import cl.guaman.design.patterns.hexagonal.domain.User;
import cl.guaman.design.patterns.hexagonal.domain.command.MessageSendCommand;
import cl.guaman.design.patterns.hexagonal.domain.port.FindUserQuery;
import cl.guaman.design.patterns.hexagonal.domain.port.SendMessagePort;
import cl.guaman.design.patterns.hexagonal.domain.validation.ObjectValidation;
import lombok.AllArgsConstructor;

/**
 * @author Fabián Guamán
 */
@AllArgsConstructor
public class NotifyUserUseCase {

    private final FindUserQuery findUserQuery;
    private final SendMessagePort sendMessagePort;
    private final ObjectValidation<MessageSendCommand> messageSendCommandObjectValidation;

    public void notify(MessageSendCommand messageSendCommand) {
        messageSendCommandObjectValidation.valid(messageSendCommand);
        User user = findUserQuery.findById(messageSendCommand.getUserId());
        sendMessagePort.send(user, messageSendCommand);
    }
}
