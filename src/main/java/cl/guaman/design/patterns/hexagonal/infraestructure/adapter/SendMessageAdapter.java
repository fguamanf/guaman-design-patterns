package cl.guaman.design.patterns.hexagonal.infraestructure.adapter;

import cl.guaman.design.patterns.hexagonal.common.enumeration.Channel;
import cl.guaman.design.patterns.hexagonal.domain.User;
import cl.guaman.design.patterns.hexagonal.domain.command.MessageSendCommand;
import cl.guaman.design.patterns.hexagonal.domain.port.SendMessagePort;
import cl.guaman.design.patterns.hexagonal.infraestructure.mock.EmailMock;
import cl.guaman.design.patterns.hexagonal.infraestructure.mock.PhoneMock;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class SendMessageAdapter implements SendMessagePort {

    private final EmailMock emailMock;
    private final PhoneMock phoneMock;

    @Override
    public void send(User user, MessageSendCommand messageSendCommand) {
        if (messageSendCommand.getChannel().equals(Channel.ALL)) {
            emailMock.send(user, messageSendCommand);
            phoneMock.send(user, messageSendCommand);
        } else if (messageSendCommand.getChannel().equals(Channel.EMAIL)) {
            emailMock.send(user, messageSendCommand);
        } else if (messageSendCommand.getChannel().equals(Channel.PHONE)) {
            phoneMock.send(user, messageSendCommand);
        }
    }
}
