package cl.guaman.design.patterns.hexagonal.common.exception;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Fabián Guamán
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ErrorVO {
    private String field;
    private String message;

    public static ErrorVO from(String field, String message) {
        return ErrorVO.builder().field(field).message(message).build();
    }
}
