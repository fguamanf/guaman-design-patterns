package cl.guaman.design.patterns.hexagonal.domain.validation;

import cl.guaman.design.patterns.hexagonal.common.enumeration.Channel;
import cl.guaman.design.patterns.hexagonal.common.enumeration.TypeMessage;
import cl.guaman.design.patterns.hexagonal.common.exception.ErrorVO;
import cl.guaman.design.patterns.hexagonal.common.exception.LogicErrorException;
import cl.guaman.design.patterns.hexagonal.domain.command.MessageSendCommand;
import lombok.AllArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author Fabián Guamán
 */
@AllArgsConstructor
public class MessageSendCommandValidation implements ObjectValidation<MessageSendCommand> {

    @Override
    public boolean valid(MessageSendCommand object) {
        List<ErrorVO> errors = new ArrayList<>();
        validNull(object, errors);
        if (!errors.isEmpty())
            throw new LogicErrorException("ERROR_FORM", errors);
        return true;
    }

    private void validNull(MessageSendCommand messageSendCommand, List<ErrorVO> errors) {
        if (Objects.isNull(messageSendCommand)) {
            errorsDefault(errors);
        } else {
            if (Objects.isNull(messageSendCommand.getUserId()))
                errors.add(ErrorVO.from("userId", "can't be null"));
            if (Objects.isNull(messageSendCommand.getChannel()))
                errors.add(ErrorVO.from("channel", "can't be null, possible values " + Channel.values().toString()));
            if (Objects.isNull(messageSendCommand.getTypeMessage()))
                errors.add(ErrorVO.from("typeMessage", "can't be null, possible values " + TypeMessage.values().toString()));
        }
    }

    private void errorsDefault(List<ErrorVO> errors) {
        errors.add(ErrorVO.from("userId", "can't be null"));
        errors.add(ErrorVO.from("channel", "can't be null, possible values " + Channel.values().toString()));
        errors.add(ErrorVO.from("typeMessage", "can't be null, possible values " + TypeMessage.values().toString()));
    }
}
