package cl.guaman.design.patterns.hexagonal.domain.port;

import cl.guaman.design.patterns.hexagonal.domain.User;
import cl.guaman.design.patterns.hexagonal.domain.command.MessageSendCommand;

/**
 * @author Fabián Guamán
 */
public interface SendMessagePort {

    void send(User user, MessageSendCommand messageSendCommand);
}
