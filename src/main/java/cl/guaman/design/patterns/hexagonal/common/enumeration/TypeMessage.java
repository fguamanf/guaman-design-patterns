package cl.guaman.design.patterns.hexagonal.common.enumeration;

public enum TypeMessage {
    SUCCESS,
    WARNING
}
