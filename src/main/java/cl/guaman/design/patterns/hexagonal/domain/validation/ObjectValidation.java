package cl.guaman.design.patterns.hexagonal.domain.validation;

/**
 * @author Fabián Guamán
 */
public interface ObjectValidation<T> {

    boolean valid(T object);
}
