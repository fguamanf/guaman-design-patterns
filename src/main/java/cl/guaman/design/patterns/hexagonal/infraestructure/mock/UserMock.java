package cl.guaman.design.patterns.hexagonal.infraestructure.mock;

import cl.guaman.design.patterns.hexagonal.infraestructure.mock.dto.UserDto;

import java.util.*;

public class UserMock {

    public List<UserDto> getAll() {
        List<UserDto> users = new ArrayList<>();
        users.add(new UserDto(UUID.randomUUID().toString(), "Jhon"));
        users.add(new UserDto(UUID.randomUUID().toString(), "Jane"));
        users.add(new UserDto(UUID.randomUUID().toString(), "Alan"));
        return users;
    }

    public Optional<UserDto> findById(String id) {
        if (id.equals("4"))
            return Optional.empty();
        return Optional.of(new UserDto(UUID.randomUUID().toString(), "Jhon"));
    }
}
