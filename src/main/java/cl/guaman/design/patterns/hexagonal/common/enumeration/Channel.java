package cl.guaman.design.patterns.hexagonal.common.enumeration;

/**
 * @author Fabián Guamán
 */
public enum Channel {
    ALL,
    EMAIL,
    PHONE
}
