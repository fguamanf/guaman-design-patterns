package cl.guaman.design.patterns.hexagonal.infraestructure.mock;

import cl.guaman.design.patterns.hexagonal.common.enumeration.TypeMessage;
import cl.guaman.design.patterns.hexagonal.domain.User;
import cl.guaman.design.patterns.hexagonal.domain.command.MessageSendCommand;

public class PhoneMock {

    public void send(User user, MessageSendCommand messageSendCommand) {
        if (messageSendCommand.getTypeMessage().equals(TypeMessage.SUCCESS)) {
            System.out.println("success phone");
        } else if (messageSendCommand.getTypeMessage().equals(TypeMessage.WARNING)) {
            System.out.println("warning phone");
        }
    }
}
