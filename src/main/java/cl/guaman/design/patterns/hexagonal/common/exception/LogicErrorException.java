package cl.guaman.design.patterns.hexagonal.common.exception;

import lombok.Getter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Fabián Guamán
 */
@Getter
public class LogicErrorException extends RuntimeException implements Serializable {
    private static final long serialVersionUID = 1905122041950251207L;
    private final List<ErrorVO> errors;

    public LogicErrorException(String message) {
        super(message);
        this.errors = new ArrayList<>();
    }

    public LogicErrorException(String message, List<ErrorVO> errors) {
        super(message);
        this.errors = errors;
    }
}
