package cl.guaman.design.patterns.hexagonal.domain;

import lombok.*;

/**
 * @author Fabián Guamán
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {

    private String id;
    private String name;
}
