package cl.guaman.design.patterns.hexagonal.infraestructure.adapter;

import cl.guaman.design.patterns.hexagonal.common.exception.LogicErrorException;
import cl.guaman.design.patterns.hexagonal.domain.User;
import cl.guaman.design.patterns.hexagonal.domain.port.FindUserQuery;
import cl.guaman.design.patterns.hexagonal.infraestructure.mock.UserMock;
import cl.guaman.design.patterns.hexagonal.infraestructure.mock.dto.UserDto;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@AllArgsConstructor
public class FindUserAdapter implements FindUserQuery {

    private final UserMock userMock;

    @Override
    public List<User> findAll() {
        List<UserDto> userDtoList = userMock.getAll();
        return userDtoList
                .stream()
                .map(item -> User.builder().id(item.getId()).name(item.getName()).build())
                .collect(Collectors.toList());
    }

    @Override
    public User findById(String id) {
        Optional<UserDto> optionalUserDto = userMock.findById(id);
        UserDto userDto = optionalUserDto.orElseThrow(() -> new LogicErrorException("user not found"));
        return new User(userDto.getId(), userDto.getName());
    }
}
