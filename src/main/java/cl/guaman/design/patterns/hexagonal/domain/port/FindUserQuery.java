package cl.guaman.design.patterns.hexagonal.domain.port;

import cl.guaman.design.patterns.hexagonal.domain.User;

import java.util.List;

/**
 * @author Fabián Guamán
 */
public interface FindUserQuery {

    List<User> findAll();

    User findById(String id);
}
