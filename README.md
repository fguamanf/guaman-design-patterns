[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![Patreon: Fabián Guamán](https://img.shields.io/badge/patreon-fabi%C3%A1n%20guam%C3%A1n-orange)](https://www.patreon.com/fguaman)
[![pipeline status](https://gitlab.com/fguamanf/guaman-design-patterns/badges/master/pipeline.svg)](https://gitlab.com/fguamanf/guaman-design-patterns/-/commits/master)
[![coverage report](https://gitlab.com/fguamanf/guaman-design-patterns/badges/master/coverage.svg)](https://gitlab.com/fguamanf/guaman-design-patterns/-/commits/master)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/6867f648dd0041ca947b8290027b31c0)](https://www.codacy.com/gl/fguamanf/guaman-design-patterns/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=fguamanf/guaman-design-patterns&amp;utm_campaign=Badge_Grade)


# Guamán Design Patterns
## Hexagonal
### Package System
```
├── cl.guaman.design.patterns.hexagonal
    └── common
        └── enumeration
        └── exception
        └── ...
    └── domain
        └── command
            └── Command.class
        └── port
            └── Port.class
            └── Query.class
        └── usecase
            └── UseCase.class
        └── Domain.class
    └── infraestructure
        └── adapter
            └── Adapter.class
        └── mock
            └── Mock.class
```